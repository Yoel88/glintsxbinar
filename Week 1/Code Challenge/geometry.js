// Function LimasSegiEmpat Argumen
function limas(panjang, lebar, tinggi) {
  console.log("Limas = ", (1 / 3) * (panjang * lebar * tinggi));
}

limas(10, 20, 3);
limas(1, 2, 30);

// Function LimasSegiEmpat Return
function LimasSegiEmpat(pjg, lbr, tng) {
  console.log("Limas Segi Empat = ", (1 / 3) * (pjg * lbr * tng));
  return (1 / 3) * (pjg * lbr * tng);
}

let LimasSegiEmpat1 = LimasSegiEmpat(1, 2, 30);
let LimasSegiEmpat2 = LimasSegiEmpat(10, 20, 3);

console.log("Jumlah Limas Return = ", LimasSegiEmpat1 + LimasSegiEmpat2);

// Function PrismaSegiTiga Argumen
function PrismaSegiTiga(pan, leb, tin) {
  console.log("Prisma = ", (1 / 2) * (pan * leb * tin));
}

PrismaSegiTiga(10, 20, 3);
PrismaSegiTiga(1, 2, 30);

// Function PrismaSegiTigaReturn
function prisma(length, width, height) {
  console.log("Prisma Segi Tiga = ", (1 / 2) * (length * width * height));
  return (1 / 2) * (length * width) * height;
}

let prisma1 = prisma(1, 2, 30);
let prisma2 = prisma(10, 20, 3);

console.log("Jumlah Prisma Return = ", prisma1 + prisma2);

console.log(
  "Jumlah Prisma1 Return dan Limas1 Return = ",
  LimasSegiEmpat1 + prisma1
);
console.log(
  "Jumlah Prisma2 Return dan Limas2 Return = ",
  LimasSegiEmpat2 + prisma2
);
