const fetch = require("node-fetch");

let urlStore = "https://fakestoreapi.com/products?limit=5";
let urlProduct = "https://fakestoreapi.com/products";
let urlJewel = "https://fakestoreapi.com/products/category/jewelery";
let data = {};

fetch(urlStore)
  .then((response) => response.json())
  .then((data) => {
    data = { response: data };
    return fetch(urlProduct);
  })

  .then((urlStore) => urlStore.json())
  .then((storeJson) => {
    data = { ...data, store: storeJson };
    return fetch(urlJewel);
  })
  .then((urlJewel) => urlJewel.json())
  .then((jewelJson) => {
    data = { ...data, jewel: jewelJson };
    console.log(data);
  })

  .catch((err) => {
    console.error(err.message);
  });
