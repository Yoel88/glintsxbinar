const express = require("express"); // Import express

const {
  getAllMotor,
  getDetailMotor,
  addMotor,
  updateMotor,
  deleteMotor,
} = require("../controller/motor"); // Import Motors controller

const router = express.Router(); // Make router

// If client request to http://localhost:3000/Motor (GET), it will go to getAllMotors function in Motor controller class
router.get("/", getAllMotor);

// If client request to http://localhost:3000/Motors/:nomor (GET), it will go to getDetailMotor function in Motor controller class
router.get("/:nomor", getDetailMotor);

// If client request to http://localhost:3000/Motors (POST), it will go to addMotor function in Motor controller class
router.post("/", addMotor);

// If client access to http://localhost/Motors/:nomor (PUT) it will go to updateMotor function in the Motors controller class
router.put("/:nomor", updateMotor);

// If client access to http://localhost/Motors/:nomor (DELETE) it will go to deleteMotor function in the Motors controller class
router.delete("/:nomor", deleteMotor);

module.exports = router; // Exports router
