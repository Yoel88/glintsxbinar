let data = require("../models/data.json"); // Import data from models

// Make motor controller class
class Motor {
  getAllMotor(req, res, next) {
    try {
      // Send response to client with status 200 (OK) and {data: data}
      res.status(200).json({ data: data });
    } catch (error) {
      // If something error it will send response to client with status 500 and {errors: ["Internal Server Error"]}
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  getDetailMotor(req, res, next) {
    try {
      // Find motor by req.params.id
      let detailData = data.filter(
        (item) => item.nomor === parseInt(req.params.nomor)
      );

      if (detailData.length === 0) {
        // Send response to client that data is not found
        return res.status(404).json({ errors: ["Motor not found"] });
      }

      // Send response to client with status 200 (OK) and {data: data}
      res.status(200).json({ data: detailData });
    } catch (error) {
      // If something error it will send response to client with status 500 and {errors: ["Internal Server Error"]}
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  addMotor(req, res, next) {
    try {
      // Get last id of motors
      let lastNomor = data[data.length - 1].nomor;

      // Add motor data to data
      data.push({
        nomor: lastNomor + 1,
        kode: req.body.kode,
        tipe: req.body.tipe,
        nama: req.body.nama,
        transmisi: req.body.transmisi,
      });

      // Send response to client with status 200 (OK) and {data: data}
      res.status(201).json({ data: data });
    } catch (error) {
      // If something error it will send response to client with status 500 and {errors: ["Internal Server Error"]}
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  updateMotor(req, res, next) {
    try {
      // Find the data is exist or not
      let findData = data.some(
        (item) => item.nomor === parseInt(req.params.nomor)
      );

      // If data not exists
      if (!findData) {
        // It will response client with status 404 and { errors: ['Motor not found'] }
        return res.status(404).json({ errors: ["Motor not found"] });
      }

      // Update data motor to data by req.params.id
      data = data.map((item) =>
        item.nomor === parseInt(req.params.nomor)
          ? {
              nomor: parseInt(req.params.nomor),
              kode: req.body.kode,
              tipe: req.body.tipe,
              nama: req.body.nama,
              transmisi: req.body.transmisi,
            }
          : item
      );

      // It will response to client with status 201 (Created) and { data: data }
      res.status(200).json({ data: data });
    } catch (error) {
      // If something error, it will return response with status 500 and { errors: ["Internal Server Error"] }
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  deleteMotor(req, res, next) {
    try {
      // Find the data is exist or not
      let findData = data.some(
        (item) => item.nomor === parseInt(req.params.nomor)
      );

      // If data not exists
      if (!findData) {
        // It will response client with status 404 and { errors: ['Motor not found'] }
        return res.status(404).json({ errors: ["Motor not found"] });
      }

      // Delete data motor to data by req.params.nomor
      data = data.filter((item) => item.nomor !== parseInt(req.params.nomor));

      // It will response to client with status 201 (Created) and { data: data }
      res.status(200).json({ data: data });
    } catch (error) {
      // If something error, it will return response with status 500 and { errors: ["Internal Server Error"] }
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }
}

module.exports = new Motor();
