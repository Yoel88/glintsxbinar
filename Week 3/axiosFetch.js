const axios = require("axios");

let urlStore = "https://fakestoreapi.com/products?limit=5";
let urlProduct = "https://fakestoreapi.com/products";
let urlJewel = "https://fakestoreapi.com/products/category/jewelery";
let data = {};

const hasilFetch = async () => {
  try {
    let response = await Promise.all([
      axios.get(urlStore),
      axios.get(urlProduct),
      axios.get(urlJewel),
    ]);

    data = {
      store: response[0].data,
      product: response[1].data,
      jewel: response[2].data,
    };

    console.log(data);
  } catch (error) {
    console.error(error.message);
  }
};

hasilFetch();
