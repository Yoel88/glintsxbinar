const fetch = require("node-fetch");

let urlStore = "https://fakestoreapi.com/products?limit=5";
let urlProduct = "https://fakestoreapi.com/products";
let urlJewel = "https://fakestoreapi.com/products/category/jewelery";
let data = {};

const hasilAsyncFetch = async () => {
  try {
    let responseStore = await fetch(urlStore); // Wait this for finish
    let responseProduct = await fetch(urlProduct);
    let responseJewel = await fetch(urlJewel);

    // Promise All
    let response = await Promise.all([
      responseStore.json(),
      responseProduct.json(),
      responseJewel.json(),
    ]);

    data = {
      store: response[0],
      product: response[1],
      jewel: response[2],
    };

    console.log(data);
  } catch (error) {
    console.error(error.message);
  }
};

hasilAsyncFetch();
