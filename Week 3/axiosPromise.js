const axios = require("axios");

let urlStore = "https://fakestoreapi.com/products?limit=5";
let urlProduct = "https://fakestoreapi.com/products";
let urlJewel = "https://fakestoreapi.com/products/category/jewelery";

let data = {};

axios
  .get(urlStore)
  .then((response) => {
    firstData = {
      price: response.data.map((item) => {
        return { id: item.id, price: item.price };
      }),
    };
    console.log(firstData);
    return axios.get(urlProduct);
  })
  .then((response) => {
    secondData = {
      category: response.data.map((item) => {
        return { id: item.id, category: item.category };
      }),
    };
    console.log(secondData);
    return axios.get(urlJewel);
  })
  .then((response) => {
    thirdData = {
      result: response.data.map((item) => {
        return { price: item.price, category: item.category };
      }),
    };
    console.log(thirdData);
  })
  .catch((err) => {
    console.error(err.message);
  });
