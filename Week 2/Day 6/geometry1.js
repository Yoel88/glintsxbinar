// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Function LimasSegiEmpat Return
function LimasSegiEmpat(pjg, lbr, tng) {
  return (1 / 3) * (pjg * lbr * tng);
}

// Function for inputing length of limas
function inputLength() {
  rl.question(`Masukkan nilai untuk Panjang Alas Limas: `, (pjg) => {
    if (!isNaN(pjg)) {
      inputWidth(pjg);
    } else {
      console.log(`Length must be a number\n`);
      inputLength();
    }
  });
}

// Function for inputing width of limas
function inputWidth(pjg) {
  rl.question(`Masukkan nilai untuk Lebar Alas Limas: `, (lbr) => {
    if (!isNaN(lbr)) {
      inputHeight(pjg, lbr);
    } else {
      console.log(`Width must be a number\n`);
      inputWidth(pjg);
    }
  });
}

// Function for inputing height of limas
function inputHeight(pjg, lbr) {
  rl.question(`Masukkan nilai untuk Tinggi Limas: `, (tng) => {
    if (!isNaN(tng)) {
      console.log(`\nVolume Limas: ${LimasSegiEmpat(pjg, lbr, tng)}`);
      rl.close();
    } else {
      console.log(`Height must be a number\n`);
      inputHeight(pjg, lbr);
    }
  });
}

console.log(`Limas Segi Empat`);
console.log(`================`);

inputLength();
