// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Function PrismaSegiTigaReturn
function prisma(length, width, height) {
  return (1 / 2) * (length * width) * height;
}

function input() {
  rl.question("Masukkan nilai untuk Alas Segitiga: ", function (length) {
    rl.question("Masukkan nilai untuk Tinggi Segitiga: ", (width) => {
      rl.question("Masukkan nilai untuk Alas Prisma:: ", (height) => {
        if (length > 0 && width > 0 && height > 0) {
          console.log(`\nVolume Prisma: ${prisma(length, width, height)}`);
          rl.close();
        } else {
          console.log(`Length, Width and Height must be a number\n`);
          input();
        }
      });
    });
  });
}

console.log(`Prisma Segi Tiga`);
console.log(`================`);

input();
