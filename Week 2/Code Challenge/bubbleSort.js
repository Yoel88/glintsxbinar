const dataAwal = [];
const angkaAcak = Math.floor(Math.random() * 100);

function arrayAwal() {
  for (let i = 0; i < angkaAcak; i++) {
    dataAwal.push(isiArrayAwal());
  }
  if (dataAwal.length == 0) {
    arrayAwal();
  }
}

function isiArrayAwal() {
  let random = Math.floor(Math.random() * 1000);
  return [null, random][Math.floor(Math.random() * 2)];
}

arrayAwal();

console.log(dataAwal);

function hapusNull(dataAwal) {
  let dataTanpaNull = [];
  for (let i = 0; i < dataAwal.length; i++) {
    if (dataAwal[i] != null) {
      dataTanpaNull.push(dataAwal[i]);
    }
  }
  return dataTanpaNull;
}
console.log(hapusNull(dataAwal));

// Fungsi Bubble Sort
function bubbleSortA(dataAwal) {
  for (let i = 0; i < dataAwal.length; i++) {
    for (let j = 0; j < dataAwal.length - i - 1; j++) {
      if (dataAwal[j] > dataAwal[j + 1]) {
        let proses = dataAwal[j];
        dataAwal[j] = dataAwal[j + 1];
        dataAwal[j + 1] = proses;
      }
    }
  }
  console.log(dataAwal);
}

bubbleSortA(hapusNull(dataAwal));

function bubbleSortD(dataAwal) {
  for (let i = 0; i < dataAwal.length; i++) {
    for (let j = 0; j < dataAwal.length - i - 1; j++) {
      if (dataAwal[j] < dataAwal[j + 1]) {
        let proses = dataAwal[j];
        dataAwal[j] = dataAwal[j + 1];
        dataAwal[j + 1] = proses;
      }
    }
  }
  console.log(dataAwal);
}

bubbleSortD(hapusNull(dataAwal));
