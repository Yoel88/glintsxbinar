/*
Create an array that contains ["tomato", "broccoli", "kale", "cabbage", "apple"]
Loop the array to print something like this "tomato is a healthy food, it's definitely worth to eat."
Exclude apple because apple is not a Vegetable.
*/

let vegetables = [`tomato`, `broccoli`, `kale`, `cabbage`, `apple`];

for (let i = 0; i < vegetables.length; i++) {
  if (vegetables[i] != "apple") {
    console.log(
      vegetables[i] + " " + "is a healthy food, it's definitely worth to eat."
    );
  } else {
    console.log(vegetables[i] + " " + "is not a Vegetable");
  }
}
